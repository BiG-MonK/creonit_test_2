import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';

ReactDOM.render(<App />, document.getElementById('root'));

// const api = new ApiService();
// api.getAllShiny().then((shiny) => shiny.forEach(a => console.log(a.title)));
// api.getAllDiski().then((diski) => diski.forEach(a => console.log(a.title)));