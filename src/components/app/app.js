import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Header from '../header';
import ShinyPage from '../pages/shiny-page';
import DiskiPage from '../pages/diski-page';
import BasketPage from '../pages/basket-page';
import './app.css';
export default class App extends Component {

  state = {
    selectedShina: null
  }

  onShinaSelected = (id) => {
    this.setState({
      selectedShina: id
    })
  }

  render () {
    return ( 
      <div>
        <Router>
          <Header />
            <Route path="/"
                   render={() => <h2>Welcome to our store</h2>}
                   exact />
            <Route path="/shiny" component={ShinyPage} />
            <Route path="/diski" component={DiskiPage} />
          <Route path="/basket" component={BasketPage} />
        </Router>
      </div>
    );
  }
}