import React, { Component } from 'react';

import ItemList from '../item-list/item-list';
import ShinyDetails from '../shiny-details';
import ApiService from '../../services/api-service';

export default class ShinyPage extends Component {

  apiService = new ApiService();

  state = {
    selectedShina: null,
    // hasError: false
  };

  // componentDidCatch(error, info) {

  //   this.setState({
  //     hasError: true
  //   });
  // }

  onShinaSelected = (id) => {
    this.setState({ selectedShina: id })
  }

  render() {

    // if (this.state.hasError) {
    //   return <ErrorIndicator />;
    // }

  return (
    <main className="main">
      <article className="main__listItems">
      <ItemList onItemSelected={this.onShinaSelected}
                getData={this.apiService.getAllShiny} />
      </article>
      <article className="main__itemDetails">
        <ShinyDetails shinaId={this.state.selectedShina}/>
      </article>
    </main>
    );
  }
}
