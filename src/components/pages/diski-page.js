import React, { Component } from 'react';
import ItemList from '../item-list/item-list';
import DiskiDetails from '../diski-details';
import ApiService from '../../services/api-service';

export default class DiskiPage extends Component {

  apiService = new ApiService();

  state = {
    selectedDisk: null,
    // hasError: false
  };

  // componentDidCatch(error, info) {

  //   this.setState({
  //     hasError: true
  //   });
  // }

  onDiskSelected = (id) => {
    this.setState({ selectedDisk: id })
  }

  render() {

    // if (this.state.hasError) {
    //   return <ErrorIndicator />;
    // }

  return (
    <main className="main">
      <article className="main__listItems">
      <ItemList onItemSelected={this.onDiskSelected}
                getData={this.apiService.getAllDiski} />
      </article>
      <article className="main__itemDetails">
        <DiskiDetails diskId={this.state.selectedDisk}/>
      </article>
    </main>
    );
  }
}