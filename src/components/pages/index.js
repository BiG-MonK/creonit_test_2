import ShinyPage from './shiny-page';
import DiskiPage from './diski-page';
import BasketPage from './basket-page';

export {
  ShinyPage,
  DiskiPage,
  BasketPage
};
