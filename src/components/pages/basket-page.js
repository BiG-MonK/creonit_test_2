import React, { Component } from 'react';
import ApiService from '../../services/api-service';

export default class BasketPage extends Component {

  apiService = new ApiService();

  state = {
    itemList: null
  }
  
  componentDidMount = () => {
    this.apiService.getBasket().then((itemList) => this.setState({itemList}))
  }

  // componentDidCatch(error, info) {
  //   this.setState({
  //     hasError: true
  //   });
  // }
  
  renderItems(arr) {
    return arr.map((item) => {
      return (
        <li className="listGroup__item" key={item.id}>
          <span className="listGroup__itemAmount">{item.amount} шт.</span>
          <img className="listGroup__itemImg" src={item.imagePreview} width="50" alt='product'/>
          <span className="listGroup__itemModel">{item.model}</span>
          <span className="listGroup__itemTotal">${item.total}</span>
          {/* <button className="itemDetails__addBasket"
                onClick={() => { 
                  this.apiService.addToBasket(item.id);
                  console.log(item.id)
                  }}>
          To buy
        </button> */}
        </li>
      )
    })
  }
  
  render() {
    const {itemList} = this.state;
    if (!itemList) { return (
                        <br/>
                      )
                    }
    const items = this.renderItems(itemList);
  
    return (
      <main className="main">
      <article className="main__listItems">
        <ul className="listGroup__items">
          {items}
        </ul>
      </article>
    </main>
    ); 
  } 
}