import React, { Component } from 'react';
import ApiService from '../../services/api-service';
import './diski-details.css';

export default class DiskiDetails extends Component {

  apiService = new ApiService();

  state = {
    disk: null
  }
  
  componentDidMount() {
    this.updateDisk();
  }

  componentDidUpdate(prevProps) {
    if (this.props.diskId !== prevProps.diskId) {
      this.updateDisk();
    }
  }

  updateDisk() {
    const { diskId } = this.props;
    if (!diskId) return;
    this.apiService
      .getDisk(diskId)
      .then((disk) => this.setState({ disk }));
  }

  render() {
    if (!this.state.disk) {
      return <span className="itemDetails__noCard">Select a disk from a list, please</span>;
    }
    const { id, model, image, price } = this.state.disk[0];
    return (
      <div className="itemDetails__card">
        <h3 className="itemDetails__title">{model}</h3>
        <div className="itemDetails__imageWrap">
          <img className="itemDetails__image" src={image} width="350" alt='product'/>
        </div>
        <span className="itemDetails__price">${price}</span>
        <button className="itemDetails__addBasket"
                onClick={() => { 
                  this.apiService.addToBasket(id);
                  document.querySelector('.itemDetails__addBasketMessage').classList.add('itemDetails__addBasketMessage--active');
                  setTimeout(() => { document.querySelector('.itemDetails__addBasketMessage').classList.remove('itemDetails__addBasketMessage--active'); }, 300);
                  }}>
          To buy
        </button>
        <span className="itemDetails__addBasketMessage"> The good added to cart ! </span>
      </div>
    )
  }
}