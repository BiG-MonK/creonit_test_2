import React, { Component } from 'react';
import './item-list.css';

export default class ItemList extends Component {

  state = {
    itemList: null
  }

  componentDidMount = () => {
    const {getData} = this.props;
    getData().then((itemList) => this.setState({itemList}))
  }

  renderItems(arr) {
    return arr.map((item) => {
      return (
        <li className="listGroup__item"
            key={item.id}
            onClick={() => { this.props.onItemSelected(item.id); }}>
          <img className="listGroup__itemImg" src={item.imagePreview} width="50" alt='product'/>
          {item.model}
        </li>
      )
    })
  }

  render() {
    const {itemList} = this.state;
    if (!itemList) { return (
                        <br/>
                      )
                    }
    const items = this.renderItems(itemList);

    return (
      <ul className="listGroup__items">
        {items}
      </ul>
    );
  }
}